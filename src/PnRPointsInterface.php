<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining PnRPoints entities.
 *
 * @ingroup points_rewards
 */
interface PnRPointsInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the PnRPoints name.
   *
   * @return string
   *   Name of the PnRPoints.
   */
  public function getName();

  /**
   * Sets the PnRPoints name.
   *
   * @param string $name
   *   The PnRPoints name.
   *
   * @return \Drupal\points_rewards\Entity\PnRPointsInterface
   *   The called PnRPoints entity.
   */
  public function setName($name);

  /**
   * Gets the PnRPoints creation timestamp.
   *
   * @return int
   *   Creation timestamp of the PnRPoints.
   */
  public function getCreatedTime();

  /**
   * Sets the PnRPoints creation timestamp.
   *
   * @param int $timestamp
   *   The PnRPoints creation timestamp.
   *
   * @return \Drupal\points_rewards\Entity\PnRPointsInterface
   *   The called PnRPoints entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the PnRPoints revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the PnRPoints revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\points_rewards\Entity\PnRPointsInterface
   *   The called PnRPoints entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the PnRPoints revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the PnRPoints revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\points_rewards\Entity\PnRPointsInterface
   *   The called PnRPoints entity.
   */
  public function setRevisionUserId($uid);

}
