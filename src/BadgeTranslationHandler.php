<?php

namespace Drupal\points_rewards;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for badge.
 */
class BadgeTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
