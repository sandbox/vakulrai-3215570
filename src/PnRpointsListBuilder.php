<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Badge entities.
 *
 * @ingroup points_rewards
 */
class PnRpointsListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Name');
    $header['name'] = $this->t('Entity Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\points_rewards\Entity\PnRPoints $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.pnr_points.edit_form',
      ['pnr_points' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
