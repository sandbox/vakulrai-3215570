<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Unicode;
use Drupal\language\Entity\ContentLanguageSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form handler for comment type edit forms.
 *
 * @internal
 */
class PnRTypeForm extends EntityForm {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a CommentTypeFormController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $callService = \Drupal::service('points_rewards.rules_and_types');
    $form['#cache']['max-age'] = 0;
    $rnp_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $rnp_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $rnp_type->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$rnp_type->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $rnp_type->getDescription(),
      '#description' => $this->t('Describe this configuration type.'),
      '#title' => $this->t('Description'),
    ];

    if ($rnp_type) {
      $options = [];
      $options = $callService->getContentEntityList(TRUE, NULL);
      $form['target_entity_type_id'] = [
        '#type' => 'select',
        '#default_value' => $rnp_type->getTargetEntityTypeId(),
        '#title' => $this->t('Target entity type'),
        "#empty_option" => $this->t('- Select -'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $rnp_type->get('target_entity_type_id'),
        '#ajax' => [
          'wrapper' => 'bundle-wrapper',
          'callback' => '::targetEntityCallback',
          'event' => 'change',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Verifying entry...'),
          ],
        ],
      ];

      $form['bundle_fieldset'] = [
        '#type' => 'container',
        '#title' => $this->t('Select Bundle'),
        '#attributes' => ['id' => 'bundle-wrapper'],
      ];

      $target_entity = !empty($form_state->getValue('target_entity_type_id')) ? $form_state->getValue('target_entity_type_id') : $rnp_type->get('target_entity_type_id');
      if (!empty($target_entity)) {
        $form['bundle_fieldset']['fieldset'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Select Bundle'),
          '#attributes' => ['style' => 'width:362px'],
        ];
        $options_bundle = [];
        $options_bundle = $callService->getContentEntityList(FALSE, $target_entity);
        $form['bundle_fieldset']['fieldset']['target_entity_type_bundle'] = [
          '#type' => 'select',
          '#default_value' => $rnp_type->get('target_entity_type_bundle'),
          '#title' => $this->t('Bundle'),
          '#empty_option' => $this->t('- Select -'),
          '#multiple' => TRUE,
          '#options' => $options_bundle,
          '#required' => TRUE,
          '#default_value' => $rnp_type->get('target_entity_type_bundle'),
          '#attributes' => [
            'id' => 'target_entity_type_bundle',
          ],
        ];
      }

      $form['action_wrapper'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Action'),
        '#attributes' => [
          'class' => 'action-wrapper',
          'style' => 'width:362px',
        ],
      ];

      $action_options = [
        'create' => 'Create',
        'update' => 'Update',
        'delete' => 'Delete',
      ];

      $form['action_wrapper']['target_entity_action'] = [
        '#type' => 'checkboxes',
        '#options' => $action_options,
        '#default_value' => !empty($rnp_type->get('target_entity_action')) ? $rnp_type->get('target_entity_action') : [],
        '#ajax' => [
          'wrapper' => 'action-wrapper',
          'callback' => '::actionTypeCallback',
          'event' => 'change',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Verifying entry...'),
          ],
        ],
        '#description' => $this->t('Select an action on which the points will be calculated.'),
      ];

      $target_action = !empty($form_state->getValue('target_entity_action')) ? $form_state->getValue('target_entity_action') : $rnp_type->get('target_entity_action');

      $form['table_definition'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'action-wrapper'],
      ];

      if (!empty($target_action) && !empty(array_filter($target_action))) {
        $header = [];
        foreach (array_filter($target_action) as $key => $value) {
          $header[$key] = Unicode::ucfirst($value);
        }

        $form['table_definition']['spt_table'] = [
          '#type' => 'table',
          '#caption' => $this->t('Add points for individual Actions'),
          '#header' => $header,
          '#empty' => $this->t('There are no items yet.', []),
          '#prefix' => '<div id="spt-fieldset-wrapper">',
          '#suffix' => '</div>',
          '#tabledrag' => [
            [
              'action' => 'order',
              'relationship' => 'sibling',
              'group' => 'table-sort-weight',
            ],
          ],
        ];
        $action = $form_state->getTriggeringElement();
        if ($action) {
          foreach ($header as $key => $value) {
            $form['table_definition']['spt_table'][0][$key] = [
              '#type' => 'number',
              '#placeholder' => 0,
              '#required' => TRUE,
              '#size' => 2,
              '#min' => 0,
            ];
          }
        }
        else {
          foreach ($rnp_type->get('spt_table')[0] as $key => $value) {
            $form['table_definition']['spt_table'][0][$key] = [
              '#type' => 'number',
              '#default_value' => !empty($rnp_type->get('spt_table')) ? $rnp_type->get('spt_table')[0][$key] : [],
              '#placeholder' => 0,
              '#required' => TRUE,
              '#size' => 2,
              '#min' => 0,
            ];
          }
        }
      }
    }

    if ($this->moduleHandler->moduleExists('content_translation')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];

      $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle('reward_point_type', $rnp_type->id());
      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'comment',
          'bundle' => $rnp_type->id(),
        ],
        '#default_value' => $language_configuration,
      ];

      $form['#submit'][] = 'language_configuration_element_submit';
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#submit' => ['test'],
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * Convert entity machine names to Labels.
   *
   * @param array $entity_type
   *   Entity type being tested.
   *
   * @return string
   *   Return a string of imploded labels.
   */
  protected function convertEntitytypeToLabel(array $entity_type) {
    foreach ($entity_type as $machine_name => $machine_name_value) {
      $labels[] = $this->entityTypeManager->getDefinition($machine_name)->getLabel();
    }
    return implode(', ', $labels);
  }

  /**
   * Wraps _comment_entity_uses_integer_id().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type being tested.
   *
   * @return bool
   *   TRUE if entity-type uses integer IDs.
   */
  protected function entityTypeSupportsComments(EntityTypeInterface $entity_type) {
    return $entity_type->entityClassImplements(FieldableEntityInterface::class) && _comment_entity_uses_integer_id($entity_type->id());
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rnp_type = $this->entity;
    $status = $rnp_type->save();
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Comment type %label has been updated.', ['%label' => $rnp_type->label()]));
    }
    else {
      $this->messenger()->addStatus($this->t('Comment type %label has been added.', ['%label' => $rnp_type->label()]));
    }

    $form_state->setRedirectUrl(Url::fromRoute('entity.reward_point_type.collection'));
  }

  /**
   * Submit callback for target entity.
   */
  public function targetEntityCallback(array $form, FormStateInterface $form_state) {
    return $form['bundle_fieldset'];
  }

  /**
   * Submit callback for action type.
   */
  public function actionTypeCallback(array $form, FormStateInterface $form_state) {
    return $form['table_definition'];
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $rnp_type = $this->entity;
    $entity = $this->entityTypeManager->getStorage('reward_point_type')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
