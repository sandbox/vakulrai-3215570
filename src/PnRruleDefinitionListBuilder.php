<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Render\Markup;

/**
 * Defines a class to build a listing of Badge entities.
 *
 * @ingroup points_rewards
 */
class PnRruleDefinitionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Reward type');
    $header['id'] = $this->t('Machine name');
    $header['summary'] = $this->t('Summary');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $summary_text = 'Milestone set for points: <b>' . $entity->get('milestone_points') . '</b><br>';
    $summary_text .= 'Type: <b>' . $entity->get('target_entity_type_id') . '</b>';
    $row['summary'] = Markup::create($summary_text);
    return $row + parent::buildRow($entity);
  }

  /**
   * Helper method to return the label of terget entity.
   *
   * @return string
   *   label of entity
   */
  protected static function getLabel($entity_id) {
    return \Drupal::service('entity_type.manager')->getDefinition($entity_id)->getLabel();
  }

}
