<?php

namespace Drupal\points_rewards;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the points_rewards term entity type.
 *
 * @see \Drupal\points_rewards\Entity\Term
 */
class PnRruleAccessHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        $access_result = AccessResult::allowedIf(!$account->hasPermission('administer reward_point_rule entities'))
          ->cachePerPermissions()
          ->addCacheableDependency($entity);
        if (!$access_result->isAllowed()) {
          $access_result->setReason("You need View permission to access the page.");
        }
        return $access_result;

      case 'update':
        if ($account->hasPermission("edit reward_point_rule entities")) {
          return AccessResult::allowed()->cachePerPermissions();
        }

        return AccessResult::neutral()->setReason("You need Edit permission for this operation.");

      case 'delete':
        if ($account->hasPermission("delete reward_point_rule entities")) {
          return AccessResult::allowed()->cachePerPermissions();
        }

        return AccessResult::neutral()->setReason("You need Delete permission for this operation.");

      default:
        // No opinion.
        return AccessResult::neutral()->cachePerPermissions();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, 'add reward_point_rule entities');
  }

}
