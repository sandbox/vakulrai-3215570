<?php

namespace Drupal\points_rewards;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a reward_point_rule type entity.
 */
interface PnRruleDefinitionInterface extends ConfigEntityInterface {

  /**
   * Returns the points_rewards type description.
   *
   * @return string
   *   The reward_point_rule-type description.
   */
  public function getDescription();

  /**
   * Sets the description of the reward_point_rule type.
   *
   * @param string $description
   *   The new description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the target entity type id for this points_rewards type.
   *
   * @return string
   *   The target entity type id.
   */
  public function getParentEntityTypeId();

  /**
   * Gets the target entity type id for this points_rewards type.
   *
   * @return string
   *   The target entity type id.
   */
  public function loadParentEntity($parent_entity_id);

  /**
   * Gets reward fids as array.
   *
   * @return string
   *   The target entity type id.
   */
  public function getFids($entity);

}
