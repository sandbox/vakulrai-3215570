<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the PnRPoints entity.
 *
 * @see \Drupal\points_rewards\Entity\PnRPoints.
 */
class PnRpointsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\points_rewards\Entity\BadgeInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished pnr_points entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published pnr_points entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit pnr_points entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete pnr_points entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add pnr_points entities');
  }


}
