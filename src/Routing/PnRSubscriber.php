<?php

namespace Drupal\points_rewards\Routing;

/**
 * @file
 * Contains \Drupal\points_rewards\Routing\PnRSubscriber.
 */

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class PnRSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.reward_point_rule.add_form')) {
      $route->setDefault('_title_callback', '\Drupal\points_rewards\Controller\PnRCallbackController::getTitle');
    }
  }

}
