<?php

namespace Drupal\points_rewards\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Badge entities.
 */
class BadgeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
