<?php

namespace Drupal\points_rewards\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\points_rewards\PnRTypeInterface;

/**
 * Defines the points_rewards type entity.
 *
 * @ConfigEntityType(
 *   id = "reward_point_type",
 *   label = @Translation("Define PnR Configuration"),
 *   label_singular = @Translation("define rnp configuration"),
 *   label_plural = @Translation("define rnp configurations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count rnp configuration type",
 *     plural = "@count rnp configuration types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\points_rewards\PnRTypeForm",
 *       "add" = "Drupal\points_rewards\PnRTypeForm",
 *       "edit" = "Drupal\points_rewards\PnRTypeForm",
 *       "delete" = "Drupal\points_rewards\PnRTypeDeleteForm",
 *     },
 *     "list_builder" = "Drupal\points_rewards\PnRTypeListBuilder",
 *     "access" = "Drupal\points_rewards\PnRtypeAccessHandler"
 *   },
 *   admin_permission = "administer reward_point_type entities",
 *   config_prefix = "type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/reward_point_type/{reward_point_type}/delete",
 *     "edit-form" = "/admin/structure/reward_point_type/{reward_point_type}",
 *     "add-form" = "/admin/structure/reward_point_type/types/add",
 *     "collection" = "/admin/structure/reward_point_type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "target_entity_type_id",
 *     "description",
 *     "target_entity_points",
 *     "target_entity_type_bundle",
 *     "target_entity_action",
 *     "spt_table",
 *   }
 * )
 */
class PnRType extends ConfigEntityBase implements PnRTypeInterface {

  /**
   * The points_rewards type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The points_rewards type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the points_rewards type.
   *
   * @var string
   */
  protected $description;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $target_entity_type_id;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type_id;
  }

}
