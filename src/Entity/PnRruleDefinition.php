<?php

namespace Drupal\points_rewards\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\points_rewards\PnRruleDefinitionInterface;

/**
 * Defines the points_rewards type entity.
 *
 * @ConfigEntityType(
 *   id = "reward_point_rule",
 *   label = @Translation("Define PnR Rules"),
 *   label_singular = @Translation("define rnp rule"),
 *   label_plural = @Translation("define rnp rules"),
 *   label_count = @PluralTranslation(
 *     singular = "@count rnp configuration rule",
 *     plural = "@count rnp configuration rules",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\points_rewards\PnRruleDefinitionForm",
 *       "add" = "Drupal\points_rewards\PnRruleDefinitionForm",
 *       "edit" = "Drupal\points_rewards\PnRruleDefinitionForm",
 *       "delete" = "Drupal\points_rewards\PnRruleDefinitionDeleteForm"
 *     },
 *     "list_builder" = "Drupal\points_rewards\PnRruleDefinitionListBuilder",
 *     "access" = "Drupal\points_rewards\PnRruleAccessHandler"
 *   },
 *   admin_permission = "administer reward_point_rule entities",
 *   config_prefix = "rule",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "parent_entity" = "parent_entity"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/reward_point_type/rule/{reward_point_rule}/edit",
 *     "add-form" = "/admin/structure/reward_point_type/reward/add",
 *     "collection" = "/admin/structure/reward_point_type/reward_point_rules",
 *     "delete-form" = "/admin/structure/reward_point_type/rule/{reward_point_rule}/delete",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "milestone_points",
 *     "target_entity_type_id",
 *     "reward_fieldset",
 *     "rewards",
 *     "rewards_value",
 *   }
 * )
 */
class PnRruleDefinition extends ConfigEntityBase implements PnRruleDefinitionInterface {

  /**
   * The points_rewards type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The points_rewards type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the points_rewards type.
   *
   * @var string
   */
  protected $description;

  /**
   * The rows of the points_rewards type.
   *
   * @var string
   */
  protected $rows;

  /**
   * The parent entity type.
   *
   * @var string
   */
  protected $parent_entity;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentEntityTypeId() {
    return $this->parent_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableRow() {
    return $this->rows;
  }

  /**
   * {@inheritdoc}
   */
  public function setTableRow($row) {
    $this->rows = $row;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function loadParentEntity($parent_entity_id) {
    return \Drupal::entityTypeManager()->getStorage('reward_point_type')->load($parent_entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getFids($entity) {
    $file_data = [];
    if (!empty($entity->get('reward_fieldset')['upcoming_rewards'])) {
      $file_data = $entity->get('reward_fieldset')['upcoming_rewards'];
      foreach ($file_data as $key => $value) {
        if (!empty($value)) {
          $file = \Drupal::entityTypeManager()->getStorage('file')->load($value['info'][0]);
          unset($file_data[$key]['info']);
          unset($file_data[$key]['points']);
          $file_data['upcoming'][$key]['fid'] = $value['info'][0];
          $file_data['upcoming'][$key]['file_uri'] = $file->getFileUri();
          $file_data['upcoming'][$key]['file_name'] = $file->getFilename();
          $file_data['upcoming'][$key]['upcoming_points'] = $value['points'];
        }
      }
    }
    if (!empty($entity->get('rewards'))) {
      $file_ = \Drupal::entityTypeManager()->getStorage('file')->load($entity->get('rewards')[0]);
      $file_data['current']['fid'] = $entity->get('rewards')[0];
      $file_data['current']['file_uri'] = $file_->getFileUri();
      $file_data['current']['file_name'] = $file_->getFilename();
      $file_data['current']['upcoming_points'] = $entity->get('milestone_points');
    }
    return $file_data;
  }

}
