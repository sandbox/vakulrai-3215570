<?php

namespace Drupal\points_rewards\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Badge entities.
 *
 * @ingroup points_rewards
 */
interface BadgeInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Badge name.
   *
   * @return string
   *   Name of the Badge.
   */
  public function getName();

  /**
   * Sets the Badge name.
   *
   * @param string $name
   *   The Badge name.
   *
   * @return \Drupal\points_rewards\Entity\BadgeInterface
   *   The called Badge entity.
   */
  public function setName($name);

  /**
   * Gets the Badge creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Badge.
   */
  public function getCreatedTime();

  /**
   * Sets the Badge creation timestamp.
   *
   * @param int $timestamp
   *   The Badge creation timestamp.
   *
   * @return \Drupal\points_rewards\Entity\BadgeInterface
   *   The called Badge entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Badge revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Badge revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\points_rewards\Entity\BadgeInterface
   *   The called Badge entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Badge revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Badge revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\points_rewards\Entity\BadgeInterface
   *   The called Badge entity.
   */
  public function setRevisionUserId($uid);

}
