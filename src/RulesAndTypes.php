<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\points_rewards\Entity\PnRPoints;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;

/**
 * Class to manage various PnR entities.
 */
class RulesAndTypes {

  /**
   * The entityTypeManager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * The user account.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  private $account;

  /**
   * The bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $bundleInfoService;

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs this ViewsData object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The language manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The account manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info_service
   *   The bundle manager.
   * @param \Drupal\Core\Database\Connection $database_connection
   *   The active database connection.
   */
  public function __construct(EntityTypeManager $entityTypeManager, AccountProxyInterface $account, EntityTypeBundleInfoInterface $bundle_info_service, Connection $database_connection) {
    $this->entityTypeManager = $entityTypeManager;
    $this->account = $account;
    $this->bundleInfoService = $bundle_info_service;
    $this->database = $database_connection;
  }

  /**
   * Gets all PnRTypes.
   *
   * @return array
   *   An array of all the PnRTypes present.
   */
  public function getTypesInfo($return_key = FALSE) {
    $info = [];
    $types = $this->entityTypeManager->getStorage("reward_point_type")->loadMultiple();
    foreach ($types as $key => $value) {
      $info[$value->getTargetEntityTypeId()] = $value->toArray();
      if ($return_key) {
        $info[$value->getTargetEntityTypeId()] = $value->getTargetEntityTypeId();
      }
    }
    return $info;
  }

  /**
   * Gets all PnRTypes.
   *
   * @return array
   *   An array of all the rules present in all entities.
   */
  public function getEntityRules() {
    $rules = $this->getRules();
    $allRules = [];
    if (empty($rules)) {
      return [];
    }
    foreach ($rules as $rule) {
      $target_entity = $this->getPnrTypes($rule->getParentEntityTypeId()) !== NULL ? array_values($this->getPnrTypes($rule->getParentEntityTypeId())->getTargetEntityTypeId())[0] : '';
      if ($target_entity == NULL) {
        continue;
      }
      $allRules[$target_entity]['id'] = $rule->id();
      $allRules[$target_entity]['parent_id'] = $rule->getParentEntityTypeId();
      $allRules[$target_entity]['target_entity'] = $target_entity;
      $allRules[$target_entity]['spt_table'] = $rule->spt_table;
    }
    return array_filter($allRules);
  }

  /**
   * Gets all Rules based on entity_type.
   *
   * @return array
   *   An array of all the rules present in a specified entity type.
   */
  public function getEntityTypeRules($entity_type, $key = NULL) {
    $allRules = $this->getEntityRules();
    if (empty($allRules)) {
      return [];
    }
    if ($key) {
      return array_key_exists($entity_type, $allRules) ? $allRules[$entity_type][$key] : [];
    }
    return array_key_exists($entity_type, $allRules) ? $allRules[$entity_type] : [];
  }

  /**
   * Create Rule Entity.
   *
   * @return entity
   *   An object of PnRPoints type on Update/Insert.
   */
  public function savePnrPoints(array $info) {
    if (empty($info)) {
      return;
    }
    $entity_ids = \Drupal::entityQuery('pnr_points')->condition('user_id', \Drupal::currentUser()->id())->execute();
    if (!empty($entity_ids)) {
      $e = $this->entityTypeManager->getStorage("pnr_points")->load(array_values($entity_ids)[0]);
      $e->set('field_total_points', $info['field_total_points']);
      $e->set('field_reward_image', array_merge($e->get('field_reward_image')->getValue(), $info['field_reward_image']));
      $e->save();
      return $e;
    }
    try {
      $entity = PnRPoints::create($info);
      $entity->save();
    }
    catch (\Exception $e) {
      return $e->getResponse();
    }
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function exist($pnr_type, $pnr_rule) {
    $query = \Drupal::entityQuery('pnr_points')->condition('user_id', $this->account->id())->condition('field_pnr_type', $pnr_type)->condition('field_pnr_rule', $pnr_rule)->execute();
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentEntityList($retrun_parent = FALSE, $parent_type = NULL) {
    $bundle_info = [];
    foreach ($this->bundleInfoService->getAllBundleInfo() as $parent_key => $parent_values) {
      foreach ($parent_values as $key => $value) {
        if ($retrun_parent) {
          $bundle_info[$parent_key] = $this->entityTypeManager->getDefinition($parent_key)->getLabel();
        }
        else {
          $bundle_info[$parent_key][$key] = is_object($value['label']) ? $value['label']->render() : $value['label'];
        }
      }
    }
    return $parent_type ? $bundle_info[$parent_type] : $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getMilestones($milestone_type) {
    $info = [];
    $milestone_entity = $this->entityTypeManager->getStorage("reward_point_rule")->loadMultiple();
    foreach ($milestone_entity as $key => $value) {
      $info[$key]['upcoimng_milestone'] = $value->get('reward_fieldset');
    }
    $info[$milestone_type]['upcoimng_milestone'][] = $value->get('rewards');
    return !empty($info) && isset($milestone_type) ? $info[$milestone_type] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function checkEntityDbRecord($where = NULL) {
    $query = $this->database->select('pnr_points_entity_statistics', 'pt');
    $query->fields('pt');
    $where ?? $query->condition('pt.entity_id', $where);
    $results = $query->execute()->fetchAllAssoc('entity_id');
    return !empty($results) ? $results : [];
  }

}
