<?php

namespace Drupal\points_rewards\Plugin\views\field;

use Drupal\views\Plugin\views\field\Date;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Field handler to display the timestamp of a point.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("pnr_points_earned_date")
 */
class PointsEarnedTime extends Date {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
  }

}
