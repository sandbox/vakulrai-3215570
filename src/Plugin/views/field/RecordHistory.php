<?php

namespace Drupal\points_rewards\Plugin\views\field;

use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Field handler to display the timestamp of a point.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("total_points")
 */
class RecordHistory extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
//   	$sql = "SELECT 
//     SUM(points) AS SUMQTY
// FROM 
//     pnr_points_entity_statistics
// GROUP BY 
//     points,entity_id";

//     $this->ensureMyTable();
//     $this->node_table = $this->query->ensureTable('pnr_points_entity_statistics', $this->relationship);
//     // Add the field.
// $params = ['aggregate' => TRUE];
// $this->field_alias = $this->query->addField(NULL, "(".$sql.")", 'pnr_points_entity_statistics', $params);
// $this->addAdditionalFields();
  }

}
