<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\views\EntityViewsData;

/**
 * Provides views data for the pnr_points entity type.
 */
class PnRpointsViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Define the base group of this table. Fields that don't have a group defined
    // will go into this field by default.
    $data['pnr_points_entity_statistics']['table']['group'] = $this->t('Points Statistics');
    $entities_types = \Drupal::entityTypeManager()->getDefinitions();
    // Provide a relationship for each entity type except pnr_points.
    foreach ($entities_types as $type => $entity_type) {
      if ($type == 'pnr_points' || !$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
        continue;
      }
      if ($type == 'user') {
        $field = 'uid';
        $extra = [];
      }
      else {
        $field = 'entity_id';
        $extra = [
          [
            'field' => 'entity_type',
            'value' => $type,
          ],
        ];
      }
      $data['pnr_points_entity_statistics']['table']['join'][$entity_type->getDataTable() ?: $entity_type->getBaseTable()] = [
        'type' => 'INNER',
        'left_field' => $entity_type->getKey('id'),
        'field' => $field,
        'extra' => $extra,
      ];
    }

    $data['pnr_points_entity_statistics']['entity_id'] = [
      'title' => $this->t('Record history'),
      'help' => $this->t('List of records created or updated by current user.'),
      'field' => [
        'id' => 'standard',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'standard',
      ],
    ];

    $data['pnr_points_entity_statistics']['created_timestamp'] = [
      'title' => $this->t('Earned date'),
      'help' => $this->t('Date and time of the record.'),
      'field' => [
        'id' => 'pnr_points_earned_date',
      ],
      'sort' => [
        'id' => 'date',
      ],
      'filter' => [
        'id' => 'date',
      ],
    ];

    $data['pnr_points_entity_statistics']['last_comment_by'] = [
      'title' => $this->t("Last record author"),
      'help' => $this->t('The name of the author of the last posted comment.'),
      'field' => [
        'id' => 'pnr_entity_last_comment_name',
        'no group by' => TRUE,
      ],
      'sort' => [
        'id' => 'pnr_entity_last_comment_name',
        'no group by' => TRUE,
      ],
    ];

    $data['pnr_points_entity_statistics']['points'] = [
      'title' => $this->t('Points count'),
      'help' => $this->t('The number of points an entity has.'),
      'field' => [
        'id' => 'numeric',
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'sort' => [
        'id' => 'standard',
      ],
      'argument' => [
        'id' => 'standard',
      ],
    ];

    $data['pnr_points_entity_statistics']['last_updated'] = [
      'title' => $this->t('Updated/commented date'),
      'help' => $this->t('The most recent of last comment posted or entity updated time.'),
      'field' => [
        'id' => 'pnr_entity_last_updated',
        'no group by' => TRUE,
      ],
      'sort' => [
        'id' => 'pnr_entity_last_updated',
        'no group by' => TRUE,
      ],
      'filter' => [
        'id' => 'pnr_entity_last_updated',
      ],
    ];

    $data['pnr_points_entity_statistics']['uid'] = [
      'title' => $this->t('Last record uid'),
      'help' => $this->t('The User ID of the author of the last record of an entity.'),
      'relationship' => [
        'title' => $this->t('Last comment author'),
        'base' => 'users',
        'base field' => 'uid',
        'id' => 'standard',
        'label' => $this->t('Last comment author'),
      ],
      'filter' => [
        'id' => 'numeric',
      ],
      'argument' => [
        'id' => 'numeric',
      ],
      'field' => [
        'id' => 'numeric',
      ],
    ];

    $data['pnr_points_entity_statistics']['entity_type'] = [
      'title' => $this->t('Entity type'),
      'help' => $this->t('The entity type to which the comment is a reply to.'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'string',
      ],
      'argument' => [
        'id' => 'string',
      ],
      'sort' => [
        'id' => 'standard',
      ],
    ];

    $data['pnr_points_entity_statistics']['action_type'] = [
      'title' => $this->t("Hook Event type"),
      'help' => $this->t('The name of the hook/event on which the record is saved.'),
      'field' => [
        'id' => 'standard',
      ],
      'filter' => [
        'id' => 'string',
      ],
      'sort' => [
        'id' => 'standard',
        'no group by' => TRUE,
      ],
    ];

    $data['pnr_points_entity_statistics']['total_points'] = [
      'title' => $this->t("Total Points"),
      'help' => $this->t('The points earned by user.'),
      'field' => [
        'id' => 'total_points',
        'no group by' => TRUE,
      ],
      'sort' => [
        'id' => 'standard',
        'no group by' => TRUE,
      ],
    ];

    return $data;
  }

}
