<?php

namespace Drupal\points_rewards\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Badge entities.
 *
 * @ingroup pnr_points
 */
class PnRpointsDeleteForm extends ContentEntityDeleteForm {


}
