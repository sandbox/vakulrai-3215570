<?php

namespace Drupal\points_rewards\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Badge entities.
 *
 * @ingroup points_rewards
 */
class BadgeDeleteForm extends ContentEntityDeleteForm {


}
