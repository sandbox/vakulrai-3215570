<?php

namespace Drupal\points_rewards;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base form handler for comment type edit forms.
 *
 * @internal
 */
class PnRruleDefinitionForm extends EntityForm {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a CommentTypeFormController.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['#cache']['max-age'] = 0;
    $rnp_type = $this->entity;
    $callService = \Drupal::service('points_rewards.rules_and_types');
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reward Name'),
      '#maxlength' => 255,
      '#default_value' => $rnp_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $rnp_type->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$rnp_type->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $rnp_type->getDescription(),
      '#description' => $this->t('Describe this configuration type.'),
      '#title' => $this->t('Description'),
      '#access' => TRUE,
    ];

    $validators = [
      'file_validate_extensions' => ['png', 'jpeg'],
    ];

    $form['milestone_points'] = [
      '#type' => 'number',
      '#title' => $this->t('Milestone'),
      '#description' => $this->t('Point at which the current milestone settings will apply.'),
      '#placeholder' => 0,
      '#size' => 2,
      '#min' => 0,
      '#default_value' => $rnp_type->get('milestone_points'),
    ];

    $options = [];
    $options = $callService->getTypesInfo(TRUE);
    $form['target_entity_type_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Target entity type'),
      "#empty_option" => $this->t('- Select type -'),
      '#options' => $options,
      '#required' => TRUE,
      '#description' => $this->t('Select a type and milestone will be assigned to the configured target entity.'),
      '#default_value' => $rnp_type->get('target_entity_type_id'),
    ];

    $form['rewards'] = [
      '#type' => 'managed_file',
      '#name' => 'users_upload',
      '#title' => 'Reward image',
      '#size' => 20,
      '#description' => 'Upload reward images.',
      '#upload_validators' => $validators,
      '#upload_location' => 'public://',
      '#required' => FALSE,
      '#tree' => TRUE,
      '#progress_message'   => t('Please wait...'),
      '#default_value' => !empty($rnp_type->get('rewards')) ? [$rnp_type->get('rewards')['fid']] : [],
    ];

    $element_add_more = $form_state->getTriggeringElement();
    if (!empty($element_add_more) && $element_add_more['#name'] == 'add-row') {
      $num_names = $form_state->getValue('rewards_value');
    }
    else {
      $num_names = $rnp_type->get('rewards_value');
    }
    $form['rewards_value'] = [
      '#type' => 'value',
      '#value' => $num_names,
    ];
    $form['#tree'] = TRUE;
    $form['reward_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Upcoming rewards'),
      '#prefix' => '<div id="reward-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_names; $i++) {
      $form['reward_fieldset']['upcoming_rewards'][$i]['points'] = [
        '#type' => 'number',
        '#title' => $this->t('Points'),
        '#placeholder' => 0,
        '#size' => 2,
        '#min' => 0,
        '#weight' => $i,
        '#default_value' => isset($rnp_type->get('reward_fieldset')[$i]['upcoming_points']) ? [$rnp_type->get('reward_fieldset')[$i]['upcoming_points']] : 0,
      ];

      $form['reward_fieldset']['upcoming_rewards'][$i]['info'] = [
        '#type' => 'managed_file',
        '#name' => 'users_upload',
        '#title' => 'Image' . $i + 1,
        '#size' => 20,
        '#description' => 'Upload reward images.',
        '#upload_validators' => $validators,
        '#upload_location' => 'public://',
        '#required' => FALSE,
        '#tree' => TRUE,
        '#weight' => $i,
        '#progress_message'   => t('Please wait...'),
        '#default_value' => isset($rnp_type->get('reward_fieldset')[$i]['fid']) ? [$rnp_type->get('reward_fieldset')[$i]['fid']] : [],
      ];
    }

    $form['reward_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['reward_fieldset']['actions']['add_name'] = [
      '#type' => 'submit',
      '#name' => 'add-row',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addMoreRow'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'reward-fieldset-wrapper',
      ],
    ];

    return $form;
  }

  /**
   * Convert entity machine names to Labels.
   *
   * @param array $entity_type
   *   Entity type being tested.
   *
   * @return string
   *   Return a string of imploded labels.
   */
  protected function convertEntitytypeToLabel(array $entity_type) {
    foreach ($entity_type as $machine_name => $machine_name_value) {
      $labels[] = $this->entityTypeManager->getDefinition($machine_name)->getLabel();
    }
    return implode(', ', $labels);
  }

  /**
   * Wraps _comment_entity_uses_integer_id().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type being tested.
   *
   * @return bool
   *   TRUE if entity-type uses integer IDs.
   */
  protected function entityTypeSupportsComments(EntityTypeInterface $entity_type) {
    return $entity_type->entityClassImplements(FieldableEntityInterface::class) && _comment_entity_uses_integer_id($entity_type->id());
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $rnp_type = $this->entity;
    $modify_fid = $rnp_type->getFids($rnp_type);
    $rnp_type->set('reward_fieldset', !empty($modify_fid['upcoming']) ? $modify_fid['upcoming'] : []);
    $rnp_type->set('rewards', $modify_fid['current']);
    $status = $rnp_type->save();
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Comment type %label has been updated.', ['%label' => $rnp_type->label()]));
    }
    else {
      $this->messenger()->addStatus($this->t('Comment type %label has been added.', ['%label' => $rnp_type->label()]));
    }

    $form_state->setRedirectUrl(Url::fromRoute('entity.reward_point_type.collection', []));
  }

  /**
   * Helper function to check whether an Example configuration entity exists.
   */
  public function exist($id) {
    $rnp_type = $this->entity;
    $entity = $this->entityTypeManager->getStorage('reward_point_rule')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['reward_fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreRow(&$form, FormStateInterface $form_state) {
    $name_field = $form_state->getValue('rewards_value');
    $add_button = $name_field + 1;
    $form_state->setValue('rewards_value', $add_button);
    $form_state->setRebuild();
  }

}
