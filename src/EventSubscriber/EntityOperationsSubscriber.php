<?php

namespace Drupal\points_rewards\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\points_rewards\Event\PnREntityEvent;
use Drupal\points_rewards\RulesAndTypes;
use Drupal\Core\Database\Connection;
use Drupal\user\EntityOwnerInterface;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\file\Entity\File;
use Drupal\Core\Config\ConfigEvents;
use Drupal\points_rewards\Entity\Badge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Logs the creation of a new node.
 */
class EntityOperationsSubscriber implements EventSubscriberInterface {

  /**
   * The entityTypeManager.
   *
   * @var Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * The entityTypeManager.
   *
   * @var Drupal\points_rewards\RulesAndTypes
   */
  private $pnrTypeManager;

  /**
   * The current database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs this ViewsData object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The language manager.
   * @param \Drupal\points_rewards\RulesAndTypes $pnrTypemanager
   *   The PnR manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The active database connection.
   */
  public function __construct(EntityTypeManager $entityTypeManager, RulesAndTypes $pnrTypemanager, Connection $database_connection) {
    $this->entityTypeManager = $entityTypeManager;
    $this->pnrTypeManager = $pnrTypemanager;
    $this->database = $database_connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('points_rewards.rules_and_types'),
      $container->get('database')
    );
  }

  /**
   * Log the creation of a new node.
   *
   * @param \Drupal\points_rewards\Event\PnREntityEvent $event
   */
  public function onCreate(PnREntityEvent $event) {
    $info = [];
    $uid = \Drupal::currentUser()->id();
    $entity = $event->getEntity();
    $pnr_type_configuration = $event->getInfo();
    $query = $this->database->insert('pnr_points_entity_statistics')
      ->fields([
        'entity_id',
        'entity_type',
        'created_timestamp',
        'last_comment_by',
        'uid',
        'points',
        'action_type',
      ]);

    $last_comment_by = 0;
    if ($entity instanceof EntityOwnerInterface) {
      $last_comment_by = $entity->getOwnerId();
    }
    if (!isset($last_comment_by)) {
      // Default to current user when entity does not implement
      // EntityOwnerInterface or author is not set.
      $last_comment_by = $uid;
    }
    $query->values([
      'entity_id' => $entity->id(),
      'entity_type' => Unicode::ucfirst($entity->getEntityTypeId()),
      'created_timestamp' => REQUEST_TIME,
      'last_comment_by' => $last_comment_by,
      'uid' => $uid,
      'points' => $pnr_type_configuration['spt_table']['0']['create'],
      'action_type' => 'Create',
    ]);
    $query->execute();
    $this->saveEntity($pnr_type_configuration, $uid, $entity);
  }

  /**
   * Log the creation of a new node.
   *
   * @param \Drupal\points_rewards\Event\PnREntityEvent $event
   */
  public function onUpdate(PnREntityEvent $event) {
    $info = [];
    $entity = $event->getEntity();
    $uid = \Drupal::currentUser()->id();
    $pnr_type_configuration = $event->getInfo();
    $query = $this->database->insert('pnr_points_entity_statistics')
      ->fields([
        'entity_id',
        'entity_type',
        'created_timestamp',
        'last_comment_by',
        'uid',
        'points',
        'action_type',
      ]);

    $last_comment_by = 0;
    if ($entity instanceof EntityOwnerInterface) {
      $last_comment_by = $entity->getOwnerId();
    }
    if (!isset($last_comment_by)) {
      // Default to current user when entity does not implement
      // EntityOwnerInterface or author is not set.
      $last_comment_by = \Drupal::currentUser()->id();
    }
    $query->values([
      'entity_id' => $entity->id(),
      'entity_type' => Unicode::ucfirst($entity->getEntityTypeId()),
      'created_timestamp' => REQUEST_TIME,
      'last_comment_by' => $last_comment_by,
      'uid' => \Drupal::currentUser()->id(),
      'points' => $pnr_type_configuration['spt_table']['0']['update'],
      'action_type' => 'Update',
    ]);
    $query->execute();
    $this->saveEntity($pnr_type_configuration, $uid, $entity);
  }

  /**
   * Log the creation of a new node.
   *
   * @param \Drupal\points_rewards\Event\PnREntityEvent $event
   */
  public function onDelete(PnREntityEvent $event) {
    $entity = $event->getEntity();
    dpm('delete');
  }

  /**
   * Saves Entity based on rules.
   *
   * @param array $pnr_type_configuration
   *   Rules configurations.
   * @param integer $uid
   *   User id.
   * @param object $entity
   *   Entity object.
   */
  public function saveEntity(array $pnr_type_configuration, $uid, $entity) {
    // Milestone.
    $milestone_ids = \Drupal::entityQuery('reward_point_rule')->condition('target_entity_type_id', (string)$pnr_type_configuration['target_entity_type_id'])->execute();
    if (!empty($milestone_ids)) {
      $achieved = [];
      $upcoming = [];
      $entity_data = [];
      $milestone_points = $this->pnrTypeManager->getMilestones(array_values($milestone_ids)[0]);
      $db = $this->database->select('pnr_points_entity_statistics', 'stat');
      $db->addExpression('SUM(points)', 'total_user_points');
      $db->condition('uid', $uid);
      $user_points = (int) $db->execute()->fetchField();
      foreach ($milestone_points['upcoimng_milestone'] as $p_key => $p_value) {
        $u_points = (int) $p_value['upcoming_points'];
        $badge_entity = Badge::create([
          'name' => 'Badge ' . $p_key,
          'field_badge' => [$p_value['fid']],
        ]);
        $badge_entity->save();
        if ($user_points >= $u_points) {
          $badge_entity->set('field_badge_type', 'achieved');
        }
        else {
          $badge_entity->set('field_badge_type', 'upcoming');
        }
        $achieved[]['target_id'] = $badge_entity->id();
        $badge_entity->save();
      }
      $entity_data['name'] = 'Entity created by ' . $entity->id();
      $entity_data['field_total_points'] = $user_points;
      $entity_data['field_reward_image'] = $achieved;
      $this->pnrTypeManager->savePnrPoints($entity_data);
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Create and modify all rule configs before import.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   The Event to process.
   */
  public function onConfigSave(ConfigCrudEvent $event, $name) {
    // Changing a theme's color settings causes the theme's asset library
    // containing the color CSS file to be altered to use a different file.
    if (strpos($event->getConfig()->getName(), 'points_rewards.rule') === 0) {
      if (!empty($event->getConfig()->get('reward_fieldset'))) {
        $fids = $event->getConfig()->get('reward_fieldset');
        foreach ($fids as $key => $value) {
          if (!isset($value['file_name']) && !isset($value['fid']) && file_exists($value['file_uri'])) {
            continue;
          }
          $file = File::create([
            'uid' => \Drupal::currentUser()->id(),
            'filename' => $value['file_name'],
            'uri' => $value['file_uri'],
            'status' => 1,
          ]);
          $file->save();
          $updated_fids[$key]['fid'] = $file->id();
          $updated_fids[$key]['file_uri'] = $file->getFileUri();
          $updated_fids[$key]['file_name'] = $file->getFilename();
        }
        $event->getConfig()->set('reward_fieldset', $updated_fids);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[PnREntityEvent::ENTITY_CREATE][] = ['onCreate'];
    $events[PnREntityEvent::ENTITY_UPDATE][] = ['onUpdate'];
    $events[PnREntityEvent::ENTITY_DELETE][] = ['onDelete'];
    $events[ConfigEvents::SAVE][] = ['onConfigSave'];
    return $events;
  }
}