<?php

namespace Drupal\points_rewards\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Represents various entity events.
 *
 * @see \Drupal\Core\Entity\EntityEvents
 * @see \Drupal\Core\Entity\EntityStorageBase::invokeHook()
 */
class PnREntityEvent extends Event {

  const ENTITY_CREATE = 'points_rewards.entity_schema_listener';
  const ENTITY_UPDATE = 'points_rewards.entity_schema_listener2';
  const ENTITY_DELETE = 'points_rewards.entity_schema_listener3';

  /**
   * The entity object.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The array PnR types.
   *
   * @var \Drupal\points_rewards\RulesAndTypes
   */
  protected $data;

  /**
   * EntityEvent constructor.
   * @param array $data
   *   The entity data.
   */
  public function __construct(array $data) {
    $this->entity = $data['entity'];
    $this->data = $data['info'];
  }

  /**
   * Returns the entity wrapped by this event.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity object.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * The array PnR types.
   *
   * @var \Drupal\points_rewards\RulesAndTypes
   */
  public function getInfo() {
    return $this->data;
  }

}
