<?php

namespace Drupal\points_rewards;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a points_rewards type entity.
 */
interface PnRTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the points_rewards type description.
   *
   * @return string
   *   The points_rewards-type description.
   */
  public function getDescription();

  /**
   * Sets the description of the points_rewards type.
   *
   * @param string $description
   *   The new description.
   *
   * @return $this
   */
  public function setDescription($description);

  /**
   * Gets the target entity type id for this points_rewards type.
   *
   * @return string
   *   The target entity type id.
   */
  public function getTargetEntityTypeId();

}
