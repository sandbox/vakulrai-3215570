<?php


/**
 * @file
 * Provide views data for comment.module.
 */

use Drupal\Core\Entity\ContentEntityInterface;
/**
 * Implements hook_views_data_alter().
 */
function points_rewards_views_data_alter(array &$data) {

  $data['pnr_points']['field_target_bundle_xyz'] = [
          'title' => t('My Entity'),
          'relationship' => [
            'group' => t('Pnr points'),
            'label' => t('Pnr Points'),
            'base' => 'pnr_points_field_data',
            'base field' => 'id',
            'relationship field' => 'id',
            'id' => 'my_entity',
            'extra' => [
              [
                'field' => 'entity_type',
                'value' => 'pnr_points',
              ],
              [
                'field' => 'field_target_bundle',
                'value' => 'field_target_bundle',
              ],
            ],
          ],
        ];
        // dpm($data);
}