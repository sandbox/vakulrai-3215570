<?php

namespace Drupal\Tests\points_rewards\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;
use Drupal\points_rewards\Entity\PnRType;

/**
 * Ensure that pnr Flow works as expected.
 *
 * @group user
 */
class AdminActionsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views', 'user', 'points_rewards', 'block',
  ];

  /**
   * List of permissions.
   *
   * @var array
   */
  protected $permissions = [
    'add reward_point_type entities', 'add reward_point_rule entities', 'add pnr_points entities', 'access administration pages', 'administer menu',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalLogin($this->drupalCreateUser([], 'admin-user', TRUE));
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Tests for database table and optional configs.
   */
  public function testConfigs() {
    $this->assertTrue(\Drupal::database()->schema()->tableExists('pnr_points_entity_statistics'), 'The database table pnr_points_entity_statistics exists.');
  }

  /**
   * Test required routes are available to admin.
   */
  public function testDefaultRoutes() {
    // Test for Pnr Type.
    $pnr_type_menu = 'entity.reward_point_type.collection';
    $this->drupalGet('/admin/structure');
    $this->assertSession()->statusCodeEquals(200);
    $pnr_type_object = Url::fromRoute($pnr_type_menu);
    $this->assertSession()->linkByHrefExists($pnr_type_object->toString());
    $link_text = t('PnR Configuration Types');
    $this->assertSession()->linkExists($link_text);
    $this->clickLink($link_text);
    $this->assertSession()->statusCodeEquals(200);

    // Test PnR add action link.
    $route = 'entity.reward_point_type.add_form';
    $pnr_type_object = Url::fromRoute($route);
    $this->assertSession()->linkByHrefExists($pnr_type_object->toString());
    // Test Milestone local task.
    $route = 'entity.reward_point_rule.collection';
    $pnr_type_object = Url::fromRoute($route);
    $this->assertSession()->linkByHrefExists($pnr_type_object->toString());
    // Test Rewar local task.
    $route = 'entity.pnr_points.collection';
    $pnr_type_object = Url::fromRoute($route);
    $this->assertSession()->linkByHrefExists($pnr_type_object->toString());
    // Test Badge local task.
    $route = 'entity.badge.collection';
    $pnr_type_object = Url::fromRoute($route);
    $this->assertSession()->linkByHrefExists($pnr_type_object->toString());
  }

  /**
   * Create config entities and test correspondig updates.
   */
  public function testAddEntity() {
    $callService = \Drupal::service('points_rewards.rules_and_types');
    $pnr_type = PnRType::create([
      'id' => 'user_type_config',
      'label' => 'User type',
      'target_entity_type_id' => 'user',
      'target_entity_type_bundle' => ['user' => 'user'],
      'target_entity_action' => ['create' => 'create'],
      'spt_table' => [['create' => 20]],
    ]);
    $pnr_type->save();
    $user_schema = [
      'name' => 'admin-xyz',
      'mail' => 'xyz@adminxyz.com',
      'roles' => [],
      'pass' => 'password123',
      'status' => 1,
    ];
    $account = User::create($user_schema);
    $account->save();
    $this->assertEquals(1, count($callService->checkEntityDbRecord($account->id())));
  }

}
