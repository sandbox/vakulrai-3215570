<?php

namespace Drupal\Tests\points_rewards\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests entity add/delete ui opertions.
 *
 * @group points_rewards
 */
class AdminEntityAddTest extends WebDriverTestBase {

  /**
   * WebAssert object.
   *
   * @var \Drupal\Tests\WebAssert
   */
  protected $webAssert;

  /**
   * DocumentElement object.
   *
   * @var \Behat\Mink\Element\DocumentElement
   */
  protected $page;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views', 'user', 'points_rewards', 'block',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->page = $this->getSession()->getPage();
    $this->webAssert = $this->assertSession();
    $this->drupalLogin($this->drupalCreateUser([], 'admin-user', TRUE));
    $this->drupalPlaceBlock('local_actions_block');
    $this->drupalPlaceBlock('local_tasks_block');
  }

  /**
   * Tests Entity add behaviour.
   */
  public function testAddEntityUi() {
    $pnr_type_menu = 'entity.reward_point_type.collection';
    $this->drupalGet('/admin/structure');
    $pnr_type_object = Url::fromRoute($pnr_type_menu);
    $this->webAssert->linkByHrefExists($pnr_type_object->toString());
    $link_text = t('PnR Configuration Types');
    $this->assertSession()->linkExists($link_text);
    $this->clickLink($link_text);
    $this->assertSession()->waitForElementVisible('named', ['link', 'Add configuration type'], 30000)->click();
    $this->page->fillField('label', 'Content type Settings');
    $this->assertSession()->waitForElementVisible('named', ['button', 'Edit'], 30000)->click();
    $this->page->fillField('id', 'node_type_point');
    $this->page->fillField('description', 'Settings for node CRUD operations');
    $this->page->fillField('target_entity_type_id', 'user');
    $this->assertSession()->waitForElementVisible('css', '#target_entity_type_bundle', 30000)->setValue('user');
    $this->page->fillField('target_entity_action[create]', 'create');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->page->fillField('spt_table[0][create]', 20);
    $this->page->fillField('target_entity_action[update]', 'update');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->page->fillField('spt_table[0][update]', 30);
    $this->page->pressButton('Save');
    // Test redirection.
    $this->assertSession()->addressEquals($pnr_type_object->toString());
  }

}
